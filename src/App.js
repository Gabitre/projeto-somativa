import React, { useState } from "react";
import "./App.css";

function App() {
  const [message, setMessage] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    const email = event.target.email.value;
    const password = event.target.password.value;
    if (email === "exemplo@gmail.com.br" && password === "123456") {
      setMessage("Acesso permitido!!");
    } else {
      setMessage("Acesso negado!!");
    }
  };

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
        <h3>Email:</h3>
        <input type="email" id="email" />
        <h3>Senha:</h3>
        <input type="password" id="password" />
        <br />
        <br />
        <button type="submit">Acessar</button>
        <h4>{message}</h4>
      </form>
    </div>
  );
}

export default App;
